// ==UserScript==
// @name        GGn Style Tweaks
// @namespace   ggn-styles
// @match       https://gazellegames.net/*
// @grant       GM_addStyle
// @version     1.0
// @author      jasperfforde
// @description Various style tweaks to make GGn nicer
// ==/UserScript==

const css = `
/* highlight selected page in collections */
.pager .selected .pageslink strong {
    font-weight: bold;
    color: var(--white);
}

/* fix cover images in collages */
.collage_images img {
    object-fit: cover;
}

.collage_images img:hover {
    opacity: .8;
}

/* fix cover images in related games */
.similar .similar_group > a img {
  object-fit: cover;
}

/* highlight autocomplete selections in torrent search */
/* also highlight in main nav searches */
#torrents #torrentsbrowsecomplete li.highlight,
#searchbars ul.autocomplete_list li.highlight {
  font-weight: bold;
}
`;

GM_addStyle(css);

// Also remove the weird space inside the hyphen in collection pages.
const links = document.querySelectorAll('.pageslink strong');

Array.from(links).forEach((el) => {
  el.textContent = el.textContent.replaceAll(/(\d+)\s*\-\s*(\d+)/g, '$1-$2');
});
