// ==UserScript==
// @name        GGn GOG DB Integration
// @description Add a GOG DB link to GGn Pages
// @version     0.0.1
// @author      jasperfforde
// @license     MIT
// @namespace   GGn-GOGDB
// @icon        https://www.gogdb.org/static/img/sizes/gogdb_48x48.png
// @run-at      document-end
// @match       *://gazellegames.net/torrents.php?id=*
// @grant       GM_addStyle
// @grant       GM_xmlhttpRequest
// @connect     www.gogdb.org
// @connect     gog.com
// ==/UserScript==

// Since we cannot do a normal fetch request because of content security policies,
// use GM_xmlhttpRequest.
const crossOriginFetch = async (url, options) => {
  return new Promise((resolve, reject) => {
    GM_xmlhttpRequest({
      url,
      ...options,
      onload: (res) => {
        try {
          resolve(JSON.parse(res.responseText));
        } catch (e) {
          reject(e);
        }
      },
      onerror: (e) => {
        reject(e);
      }
    })
  })
}

// Extract game info from GGn page.
const getGGnInfo = () => {
  try {
    const fullTitle = document.querySelector('#display_name').textContent;
    // Get the title and year of release.
    const [_, platform, title, year] = fullTitle.match(/(.*?)\s\-\s(.*?)\s\((\d{4})\)/);

    return { platform, title, year };
  } catch (e) {
    console.warn(`[GOG-DB] Cannot parse game title: ${e.message}`);
  }
}

// Fetch game from GOG. Because GOG's title matching isn't very good, we fetch
// the first 5 results and then try to exactly match the title on one of them.
const fetchFromGOG = async (title, year) => {
  try {
    const json = await crossOriginFetch(
      `https://embed.gog.com/games/ajax/filtered?mediaType=game&search=${title.toLowerCase()}&release=${year}&limit=5&sort=title`
      , {
        mode: 'cors',
        credentials: 'omit'
      }
    );

    if (json.products.length) {
      return json.products.find((p) => p.title.toLowerCase() === title.toLowerCase());
    }

    throw new Error('Game not found!');
  } catch (e) {
    console.warn(`[GOG-DB] Cannot fetch game "${title} (${year})" from GOG: ${e.message}`);
  }
}

// Get GOG ID from game info by querying GOG's API with the title and year of release.
const getGOGId = async ({ title, year }) => {
  const gogInfo = await fetchFromGOG(title, year);

  console.log(`[GOG-DB] Found "${title}" on GOG:`, gogInfo);

  return gogInfo.id;
}

// Get latest version (for the specified platform) from GOGDB.
const getLatestVersion = async (gogId, { platform }) => {
  // Convert GGn platforms to GOG values.
  const platforms = {
    Windows: 'windows',
    Mac: 'osx',
    Linux: 'linux'
  };
  const gogPlatform = platforms[platform];

  try {
    const json = await crossOriginFetch(
      `https://www.gogdb.org/data/products/${gogId}/product.json`
      , {
        mode: 'cors',
        credentials: 'omit'
      }
    );

    const latestBuild = json.builds.reverse().find((b) => {
      // Only return builds that match the platform and have version
      // numbers without extra info (e.g. only 1.2.3, not COMPAT-1.2.3)
      return b.os === gogPlatform && b.version.match(/^[\d\.]+$/);
    });

    if (latestBuild) {
      return {
        // Remove extra info after version number, e.g. "(GOG)" tag
        version: latestBuild.version.replace(/\s.*$/, ''),
        date: latestBuild.date_published
      };
    }

    // Often, linux releases are not in the builds list. We can get the latest
    // version from the installers list, but it won't have release date info.
    const latestInstaller = json.dl_installer.find((i) => i.os === gogPlatform);

    if (latestInstaller) {
      return {
        version: latestInstaller.version
      };
    }

    throw new Error(`No ${platform} version found!`);
  } catch (e) {
    console.warn(`[GOG-DB] Cannot fetch game with GOG ID "${gogId}" from GOGDB: ${e.message}`);
  }
}

// Quick and dirty date formatting. Change this:
// 2017-09-20T11:38:55+00:00
// into this:
// Sep 20, 2017 at 11:38
const formatDate = (date) => {
  const [_, y, mo, d, h, mi] = date.match(/(\d{4})\-(\d{2})\-(\d{2})T(\d{2}):(\d{2})/);
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  const monthIndex = parseInt(mo) - 1;
  const month = months[monthIndex];

  return `${month} ${d}, ${y} at ${h}:${mi}`;
}

// Add GOG Database panel to left sidebar.
const addPanel = (gogId, { version, date }) => {
  const box = document.createElement('div');
  const head = document.createElement('div');
  const headTitle = document.createElement('strong');

  const body = document.createElement('div');
  const linkLabel = document.createElement('strong');
  const link = document.createElement('a');
  const br1 = document.createElement('br');
  const versionLabel = document.createElement('strong');
  const versionText = document.createElement('span');
  const br2 = document.createElement('br');
  const releaseLabel = document.createElement('strong');
  const release = document.createElement('span');

  box.classList.add('box', 'box_gog');
  head.classList.add('head');
  body.classList.add('body');

  headTitle.textContent = 'GOG Database';
  head.appendChild(headTitle);

  linkLabel.textContent = 'GOG DB: ';
  link.setAttribute('href', `https://www.gogdb.org/product/${gogId}`);
  link.setAttribute('_target', 'blank');
  link.textContent = 'link';
  versionLabel.textContent = 'Latest Version: ';
  versionText.textContent = version;
  releaseLabel.textContent = 'Released: ';
  release.textContent = date ? formatDate(date) : 'Unknown';
  body.appendChild(linkLabel);
  body.appendChild(link);
  body.appendChild(br1);
  body.appendChild(versionLabel);
  body.appendChild(versionText);
  body.appendChild(br2);
  body.appendChild(releaseLabel);
  body.appendChild(release);

  box.appendChild(head);
  box.appendChild(body);

  document.querySelector('.sidebar').appendChild(box);
}

// Add the "Latest Version" tag to the relevant torrent(s), if they exist.
const addTorrentTag = ({ version }) => {
  const torrents = Array.from(document.querySelectorAll('.group_torrent a'));
  const latestTorrents = torrents.filter((t) => t.textContent.includes(version));

  latestTorrents.forEach((el) => {
    const tag = document.createElement('strong');
    const endBracket = document.createTextNode(']');

    tag.setAttribute('style', 'color: #07d100;');
    tag.textContent = 'Latest Version';

    el.textContent = el.textContent.replace(']', ', ');
    el.appendChild(tag);
    el.appendChild(endBracket);
  });
}

// Main function that gets called when the page renders. It fetches GOG info
// about the game, then adds the "GOG Database" panel and marks a torrent
// with the "Latest Version" tag, if applicable.
const main = async () => {
  // First, get the game title and year from the GGn page.
  const info = getGGnInfo();

  if (!info) return;

  // Use that info to fetch the game from GOG's API.
  const gogId = await getGOGId(info);

  if (!gogId) return;

  // If we've found the game on GOG, look up its ID on GOGDB to find the
  // version information.
  const latestVersion = await getLatestVersion(gogId, info);

  if (!latestVersion) return;

  // Add the "GOG Database" panel to the page.
  addPanel(gogId, latestVersion);

  // Add the "Latest Version" tag to the torrent, if applicable.
  addTorrentTag(latestVersion);
}

main();
